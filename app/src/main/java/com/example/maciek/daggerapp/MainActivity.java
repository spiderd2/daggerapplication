package com.example.maciek.daggerapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListItem> listItems;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TestComponent component = DaggerTestComponent.builder()
                .testModule(new TestModule())
                .contextModule(new ContextModule(this))
                .build();

        listItems = component.getListItems();
        recyclerView =component.getRecyclerView();
        adapter = component.getAdapter();


    }
}
