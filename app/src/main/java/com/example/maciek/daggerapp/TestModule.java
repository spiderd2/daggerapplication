package com.example.maciek.daggerapp;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;


@Module(includes = ContextModule.class)
public class TestModule {


    @Provides
    public RecyclerView recyclerView(Context context,RecyclerView.Adapter adapter ){


        View rootView = ((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content);
        RecyclerView recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(adapter);
        return recyclerView;
    }

    @Provides

    public RecyclerView.Adapter adapter(List<ListItem> listItems, Context context){

        return new MyAdapter(listItems, context);
    }

    @Provides
    public List<ListItem> listItems(){
        List<ListItem> listItems=new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ListItem listItem = new ListItem(
                    "Naglowek "+ (i+1),
                    "Opis bla bla bla"
            );

            listItems.add(listItem);

        }
        return listItems;
   }
}
