package com.example.maciek.daggerapp;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Maciek on 24.10.2017.
 */
@Module
public class ContextModule {
    private final Context context;

    public ContextModule(Context context){
        this.context = context;
    }

    @Provides
    public Context context(){
        return context;
    }
}
