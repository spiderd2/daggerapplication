package com.example.maciek.daggerapp;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import dagger.Component;

@Component(modules = TestModule.class)
public interface TestComponent {


    RecyclerView getRecyclerView();

    RecyclerView.Adapter getAdapter();

    List<ListItem> getListItems();
}
